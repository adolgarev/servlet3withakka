package test;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.actor.ReceiveTimeout;
import akka.actor.Terminated;
import akka.actor.UntypedActor;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.AsyncContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import scala.concurrent.duration.Duration;

@WebServlet(urlPatterns = "/test/akka", asyncSupported = true)
public class TestAkkaServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        final AsyncContext asyncContext = req.startAsync();
        final ActorSystem system = (ActorSystem) req.getServletContext()
                .getAttribute("ActorSystem");
        system.actorOf(Props.create(AskActor.class, asyncContext));
    }

    static class TestActor extends UntypedActor {

        @Override
        public void onReceive(Object msg) throws Exception {
            if (msg == "Test!!!") {
                getSender().tell("Ok", getSelf());
            } else {
                unhandled(msg);
            }
        }
    }

    static class AskActor extends UntypedActor {

        final private AsyncContext asyncContext;

        public AskActor(AsyncContext asyncContext) {
            this.asyncContext = asyncContext;
            ActorRef testActor = getContext()
                    .actorOf(Props.create(TestActor.class), "TestActor");
            getContext().watch(testActor);
            getContext().setReceiveTimeout(Duration.create("5 seconds"));
            testActor.tell("Test!!!", getSelf());
        }

        @Override
        public void onReceive(Object msg) throws IOException {
            HttpServletResponse resp = (HttpServletResponse) asyncContext.getResponse();
            if (msg instanceof ReceiveTimeout) {
                getContext().stop(getSelf());
                resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
                        "Timeout");
                asyncContext.complete();
            } else if (msg instanceof Terminated) {
                getContext().stop(getSelf());
                resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
                        "Unexpectedly Stopped");
                asyncContext.complete();
            } else if (msg instanceof String) {
                getContext().stop(getSelf());
                resp.setContentType("text/plain");
                try (PrintWriter writer = resp.getWriter()) {
                    writer.print("Ok");
                }
                asyncContext.complete();
            } else {
                unhandled(msg);
            }
        }

    }

}
