package test;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.AsyncContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = "/test/async", asyncSupported = true)
public class TestAsyncServlet extends HttpServlet {

    private Executor executor;

    @Override
    public void init() throws ServletException {
        executor = Executors.newFixedThreadPool(30);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        final AsyncContext asyncContext = req.startAsync();
        executor.execute(new Runnable() {

            @Override
            public void run() {
                HttpServletResponse resp = (HttpServletResponse) asyncContext.getResponse();
                resp.setContentType("text/plain");
                try (PrintWriter writer = resp.getWriter()) {
                    writer.print("Ok");
                } catch (IOException ex) {
                    Logger.getLogger(TestAsyncServlet.class.getName()).log(Level.SEVERE, null, ex);
                }
                asyncContext.complete();
            }
        });
    }
}
