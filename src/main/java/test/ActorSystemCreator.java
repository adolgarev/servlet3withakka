package test;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import akka.actor.ActorSystem;

@WebListener
public class ActorSystemCreator implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        ActorSystem system = ActorSystem.create("Test");
        sce.getServletContext().setAttribute("ActorSystem", system);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        ActorSystem system = (ActorSystem) sce.getServletContext().getAttribute("ActorSystem");
        sce.getServletContext().removeAttribute("ActorSystem");
        system.shutdown();
    }
}
